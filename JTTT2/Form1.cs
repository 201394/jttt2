﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Program został uruchomiony z parametrami: \r\n URL: " + URL.Text + " \r\n Tekst: " + Tekst.Text + "\r\n Email: " + Email.Text);

            var hs = new html(URL.Text,Tekst.Text);

            //MessageBox.Show(hs.WritePageNodes());

            var test = new Wysylanie(hs.WritePageNodes(),Email.Text);

            try
            {
                test.WyslijWiadomosc();
            }
            catch
            {
                MessageBox.Show(this, "Nie udało sie wysłać wiadomości", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Console.Read();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
