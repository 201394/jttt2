﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;

namespace JTTT2
{
    public class html
    {
        private string url;
        private string tekst;

        public html(string url, string tekst)
        {
            this.url = url;
            this.tekst = tekst;
        }

        public string PobierzHtml()
        {
            using (WebClient wc = new WebClient())
            {
                string zawartosc = wc.DownloadString(url);
                return zawartosc;
            }
        }

        public string WritePageNodes()
        {
            // Tworzymy obiekt klasy HtmlDocument zdefiniowanej w namespace HtmlAgilityPack

            HtmlDocument doc = new HtmlDocument();

            string pageHtml = PobierzHtml();

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            string strona = "";
            foreach (var node in nodes)
            {
                string tytul = node.GetAttributeValue("alt", "");
                
                int tmp = 0;
                tmp = tytul.IndexOf(tekst);
                if (tmp != -1)
                {
                    strona = node.GetAttributeValue("src", "");  //zwraca adres url do zdjecia
                }                
            }
            return strona;
        }
    }
}
